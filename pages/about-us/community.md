---
title: Community
description: Join our community of developers. 
layout: subpage
weight: 3
---


We want to build a community people who want a better way to connect and communicate. 
We want to bring together folks to help build great applications using the Veilid framework.

<a class="mt-3 btn btn-primary btn-lg" href="/discord">Join Us on Discord</a>

