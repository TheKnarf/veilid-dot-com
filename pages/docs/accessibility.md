---
title: Accessibility
description: An introduction to accessibility concepts and resources
weight: 30
layout: subpage
---

[toc]

### General Accessibility Concepts

For the scope of this document, we are referring to the accessibility of websites, documents, and applications.

### Types of Accessibility Challenges

Here is a partial list of capabilities to keep in mind when designing:

**Fine Motor Control** – The dexterity of one's eyes, hands, and fingers for the ability to examine and manipulate objects. In practical terms, is it easy for someone using a touch screen to easily click interactive elements and can folks with fine motor control issues manage to interact with those elements was well.

**Sightedness** – Levels of blindness, near sightedness, and far sightedness. Low-vision users tend to need high color contrast and larger sized text and elements.

**Color Vision** – Color blindness changes how people perceive colors which can reduce color contrast and make differentiating color coded items difficult.

**Cognitive Capabilities** – Memory, train of thought, following instructions, and judgment are cognitive capabilities that most folks take for granted. Various disabilities can affect these as well as situational conditions such as being tired from working late.

**Hearing** – Hearing difficulties can make audio cues or videos less effective.

### Accessibility Guidelines

Here is a partial list of general accessibility goals you should strive for:

#### Structure

- Headers are marked denoted at headers in the mark-up or code
- Sections of a page or screen are visually distinct and easy to identify

#### Navigation

- Easy to figure out current location
- Easy to figure out where you can go
- Consistent navigation structures and styling

#### Interactions

- Clickable items are styled consistently as clickable
- Clickable items have a generous hit-box size
- UI elements can be easily manipulated with assistive technology

#### Typography

- Adjustable text size
- Adjusting the text size does not breaking the layout or usability
- Text customization options such as changing fonts

#### Colors

- Good color contrast for text to be readable
- Use a secondary indicator (shape, size, pattern, etc) rather than color alone

#### Video

- For the audio in videos, provide subtitles of the dialog and sounds
- A transcript of all the content conveyed in the video helps a wide audiences

### Additional Reading

- [Mozilla's Accessibility Reference Pages](https://developer.mozilla.org/en-US/docs/Web/Accessibility)
- [Cognitive Accessibility at W3C ](https://www.w3.org/WAI/cognitive/)

### Usability Tools

- [Color Contrast Grid](https://contrastgrid.com/)
- [WebAIM WAVE](https://wave.webaim.org/)
