---
title: Framework Overview
description: An overview of the Veilid Framework
weight: 20
layout: subpage
sortby: weight
---

<div class="focus-text">
Veilid is an open-source, peer-to-peer, mobile-ﬁrst, networked application framework.
</div>

The framework is conceptually similar to <a href="https://ipfs.tech/">IPFS</a> and <a href="https://www.torproject.org/">Tor</a>, but faster and designed from the ground-up to provide all 
services over a privately routed network.

The framework enables development of fully-distributed applications without a 'blockchain' or 
a 'transactional layer' at their base.

The framework  can be included as part of user-facing applications or run as a 'headless node' 
for power users who wish to help build the network.

Download the PDF [Slides from Defcon](/Launch-Slides-Veilid.pdf)

